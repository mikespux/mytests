-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2016 at 12:55 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `citizens`
--

-- --------------------------------------------------------

--
-- Table structure for table `citizen_details`
--

CREATE TABLE IF NOT EXISTS `citizen_details` (
  `ID` varchar(100) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `Age` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `Country` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citizen_details`
--

INSERT INTO `citizen_details` (`ID`, `FullName`, `Age`, `Gender`, `Country`) VALUES
('23003355', 'Sophia Njeru', '25', 'Female', 'Kenya'),
('25689521', 'Jennifer Mdee', '21', 'Female', 'Tanzania'),
('25866236', 'Michael Orengo', '32', 'Male', 'Kenya'),
('28885622', 'Kelvin Omai', '25', 'Male', 'Malawi'),
('30023536', 'Orengo Michael', '21', 'Male', 'Kenya'),
('30025363', 'Sophia Njeri', '21', 'Female', 'Egypt'),
('30050432', 'Njeri Sophia', '21', 'Female', 'Malawi'),
('30050630', 'Joseph Kioni', '22', 'Male', 'Kenya'),
('30050631', 'Ruth Oprah', '35', 'Female', 'Egypt'),
('30050634', 'Michael Orenge', '22', 'Male', 'Kenya');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
