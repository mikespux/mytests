<?php 
require 'config.php';
error_reporting(0);
include $_SERVER["DOCUMENT_ROOT"].'/searchfilter/includes/overall/header.php';

?>
<!DOCTYPE html>

<html >

  <head>
    <meta charset="UTF-8">
    <title>Filtered Search</title>
    
    
    <link rel="stylesheet" href="css/normalize.css">

    
        <style>
     
      * {
  box-sizing: border-box;

}
body {
  padding: 20px;
  width: 70%;
  margin: 0 auto;

}
.screen-reader-text {
  position: absolute;
  top: -9999px;
  left: -9999px;
}

.search-form {
  width: 35%;
  float: left;
  padding-right: 20px;

}
.search-term {
  width: 100%;
  height:43px;
  margin: 0 0 5px 0;
   background: #464545;
   border:1px solid #f1f1f1;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-webkit-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s
}
.search-terms {
  display: table;
  margin: 0 0 10px 0;

}
.search-filters {
  overflow: hidden;
  margin: 0 0 10px 0;
   background: #141d27;
  padding: 10px;
}
.search-filters-title {
  font-weight: normal;
  font-size: 1em;
  margin: 0 0 10px 0;
}
.filter-group {
  margin: 0 0 10px 0;
}
.cloned-filters {
  display: none; 
}
.search-results {
  width: 65%;
  float: right;
  padding-bottom: 2500px; /* just scrolling space */
}
.result {
  float: left;
  width: 100%;
  height: 100px;
  margin-right: 1.33%;
  margin-bottom: 1.33%;
  background: #464545;
}
.result:nth-child(4n) {
  margin-right: 1.33%; 
}
.result img {
  width: 100%;
  height: 100%;
  display: block;
}
.contain {
  max-width: 100%;
  margin: 0 0 5px 0;
    margin-right: 1.33%;

   background: #464545;
   border:1px solid #464545;border-radius:4px;
  
}

@media (max-width: 1000px) {
  body {
    width: 90%;
  }
}

@media (max-width: 600px) {
  body,
  .search-form,
  .search-results {
    width: 100%;
  }
  .search-form {
    padding: 0;
  }
  .search-terms {
    display: table;
    width: 100%;
    margin: 0;
  }
  .search-terms > div {
    display: table-row; 
  }
  .search-terms > div > span {
    display: table-cell;
  }
  .search-term-wrap {
    padding-right: 10px;
     

  }
  .search-button {
    width: 100%;

  }
  .search-filters-title {
    color: #fff;
        cursor: pointer;
    margin: 0;
  }
  .search-filters.pinned {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background: rgba(0, 0, 0, 0.2);
  }
  .filter-group {
    display: none;
  }
  .filter-explanation {
    display: none;  
  }
  
  .filters-open .filter-group,
  .filters-open .filter-explanation {
    display: block; 
  }
  .filters-open .search-filters-title {
    margin: 0 0 10px 0;
  }
}
    </style>

    
        <script src="js/responsive.js"></script>

    
  </head>


  <body>
    <div class="row">

    <form role="search" class="search-form" id="search-form" action="index.php" method="post">
  
  <section class="search-terms">
    <label for="search-term" class="search-term-label screen-reader-text">Search Terms</label>
    <div>
      <span class="search-term-wrap">
        <input type="search" id="search-term" name="string" class="search-term" placeholder="Enter name or IDNo..."
        value="<?php echo $_REQUEST["string"]; ?>" >
      </span>


      <span class="search-term-button-wrap">
        <input type="submit" value="Search" class="btn btn-primary">&nbsp;&nbsp;
       
      </span>
         <span class="search-term-button-wrap">
      
         <a href="index.php" class="btn btn-primary"> Clear</a>
      </span>


    </div>
    <br>
    <section class="search-filters" id="search-filters">
    
    <h3 class="search-filters-title" id="search-filters-title">Filter By</h3>
     <div>
     <small class="filter-explanation">By Age.</small>
      <span class="search-term-wrap">
        <input type="search" id="search-term" name="age" class="search-term" placeholder="Enter Age..."
        value="<?php echo $_REQUEST["age"]; ?>">
      </span>
      </div>
    <div >
    

    
    <div  >
     <small class="filter-explanation">By Gender.</small>
     
      <div class="search-term-wrap">
        <select name="gender" class="form-control"
        >
        <option value="">By Gender...</option>
        <option>Male</option>
         <option>Female</option>
         </select> 
          </div>
<br>
              <div >
     <small class="filter-explanation">By Country.</small>
     
      <div class="search-term-wrap">
        <select name="country" class="form-control">
        <option value="">By Country...</option>
        <option>Kenya</option>
         <option>Malawi</option>
          <option>Tanzania</option>
          <option>Egypt</option>
         </select> 
          </div>
   
     
   </div>
    
   
    
  </section>
  </section>

  
</form>

<section class="search-results">


      <table width="700" border="0" cellspacing="0" cellpadding="4" >
  
<?php

// Specify the query to execute

if ($_REQUEST["string"]<>'') {
  $search_string = "AND (FullName LIKE '%".mysql_real_escape_string($_REQUEST["string"])."%' OR ID LIKE '%".mysql_real_escape_string($_REQUEST["string"])."%')"; 
}
if ($_REQUEST["age"]<>'') {
  $search_age = "AND (Age LIKE '%".mysql_real_escape_string($_REQUEST["age"])."%')"; 
}
if ($_REQUEST["gender"]<>'') {
  $search_gender = "AND (Gender ='".mysql_real_escape_string($_REQUEST["gender"])."')"; 
}
if ($_REQUEST["country"]<>'') {
  $search_country = "AND (Country ='".mysql_real_escape_string($_REQUEST["country"])."')"; 
}

$sql = "select * from citizen_details where ID>0 ".$search_string.$search_age.$search_gender.$search_country;
$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
if (mysql_num_rows($sql_result)>0) {
  while ($row = mysql_fetch_assoc($sql_result)) {

?> <div class="col-lg-8">
      
    
       
 
    <div class="result" >
    <img src="images/icon-profile.png" alt="" align="left" style="width:83px;height:80px;" />

    &nbsp;&nbsp;<?php echo $row["FullName"]; ?><br/>
    &nbsp;&nbsp;<?php echo $row["ID"]; ?><br/>
   &nbsp;&nbsp;<?php echo $row["Age"]; ?> <?php echo $row["Gender"]; ?> from
    <?php echo $row["Country"]; ?>

</div>
  </div>
     
<?php
  }

} else {
?>
  </td>

  </tr>
<tr><td colspan="5">No results found.</td>
<?php 
}
?>
</section>

  
      
        <script src="js/index.js"></script>
</div>
  </body>
</html>
